#!/bin/sh

# Install Chromium
xset -dpms
xset s off
xset s noblank

unclutter &
chromium-browser http://localhost/#/tv --start-fullscreen --kiosk --incognito --noerrdialogs --disable-translate --no-first-run --fast --fast-start --disable-infobars --password-store=basic --disable-features=TranslateUI --disk-cache-dir=/dev/null
