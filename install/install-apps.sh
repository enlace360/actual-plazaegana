cd /home/freshwork
git clone https://bitbucket.org/freshworkstudio/actual.git
git clone https://bitbucket.org/freshworkstudio/actual-socket.git

# Install actual-socket dependencies
cd actual-socket
yarn

# Install actual laravel dependencies and configurations
cd ../actual
composer install
cp .env.example .env
sed -i 's/DB_PASSWORD=secret/DB_PASSWORD=CHTEglezJ7bhvw4ahv3G/' .env
sed -i 's/BACKUP_TOKEN=/BACKUP_TOKEN=6Y2F8VPs28XsqKp636sXW2CN3/' .env
php artisan key:generate
php artisan migrate
php artisan db:seed
php artisan storage:link
yarn
yarn production

line="* * * * * php /home/freshwork/actual/artisan schedule:run"
(crontab -u freshwork -l; echo "$line" ) | crontab -u freshwork -
