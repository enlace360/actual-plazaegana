<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Gonzalo De Spirito',
            'email' => 'gonzunigad@gmail.com',
            'password' => bcrypt('secret')
        ]);
    }
}
