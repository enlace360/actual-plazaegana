<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $query = Category::query()->where('visible_on_ipad', true)->orderBy('order', 'ASC');
        if ($store_id = config('app.store_id')) {
            $query = $query->where('store_id', $store_id);
        }
        return response()->json(['data' => $query->get()]);
    }
}
