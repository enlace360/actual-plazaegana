<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Media
 * @package App
 */
class Media extends Model
{
    /**
     * @var array
     */
    protected $guarded = ['id'];
    /**
     * @var array
     */
    protected $touches = ['category'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}
