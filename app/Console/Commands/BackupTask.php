<?php

namespace App\Console\Commands;

use App\Backup;
use Illuminate\Console\Command;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class BackupTask extends Command
{
    protected $zipName;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run backup task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->backup();
    }

    /**
     * @throws \Exception
     */
    protected function backup()
    {
        $this->zipName = Uuid::uuid4()->toString() . '.zip';

        $this->zipFiles()
            ->dumpDb()
            ->zipDb();

        Backup::create(['name' => $this->zipName]);
    }

    /**
     * @return $this
     */
    protected function zipDb()
    {
        $process = new Process([
            'zip',
            '-j',
            storage_path('backups/' . $this->zipName),
            'db.sql'
        ], storage_path('app'));

        $this->info('Command: ' . $process->getCommandLine());

        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $this->info('DB Dumped successfully');

        return $this;
    }

    /**
     * @return $this
     */
    protected function dumpDb()
    {
        $process = new Process([
            'mysqldump',
            '-u' . env('DB_USERNAME'),
            '-p' . env('DB_PASSWORD'),
            '-c',
            '-e',
            '--default-character-set=utf8',
            '--single-transaction',
            '--skip-set-charset',
            '--add-drop-database',
            env('DB_DATABASE'),
        ], storage_path('app'));

        $this->info('Command: ' . $process->getCommandLine());

        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        file_put_contents(storage_path('app/db.sql'), $process->getOutput());

        $this->info('DB Dumped successfully');

        return $this;
    }

    /**
     * @return mixed
     */
    protected function zipFiles()
    {
        $process = new Process(['zip', '-r', storage_path('backups/' . $this->zipName), 'public'], storage_path('app'));
        $this->info('Command: ' . $process->getCommandLine());

        if (!file_exists(storage_path('backups'))) {
            mkdir(storage_path('backups'));
        }

        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $this->info('Backup created successfully: ' . $this->zipName);

        return $this;
    }
}
