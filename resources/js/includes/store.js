import Vue from 'vue';
import Vuex from 'vuex'
Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        store: {}
    },
    mutations: {
        store (state, store) {
            state.store = store
            document.title = state.store.name + ' - Actual'
        }
    },
    getters: {
        backToHomeTime(state, getters) {
            let storeTimeout = state.store.back_to_home_timeout;
            if (!storeTimeout) {
                storeTimeout = 60;
            }
            storeTimeout *= 1000;
            return storeTimeout;
        }
    }
})

export {store}
