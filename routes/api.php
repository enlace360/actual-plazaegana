<?php

use App\Category;
use App\Media;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/store', function (Request $request) {
    if (!$store_id = config('app.store_id')) {
        return response()->json(['data' => null]);
    }
    return response()->json(['data' => \App\Store::find(config('app.store_id'))]);
});

Route::get('/categories', 'CategoryController@index');

Route::get('/media', function (Request $request) {
    $intro_media = null;
    $intro_category = \App\Store::find(config('app.store_id'))->slider_category;
    if ($intro_category) {
        $intro_media = $intro_category->media;
    }
    return response()->json(['data' => ['media' => Media::get(), 'intro' => $intro_media]]);
});

Route::get('/categories/{category}/media', function (Request $request, Category $category) {
    return response()->json(['data' => $category->load('media')]);
});

Route::get('/sync', 'SyncDataController@sync');
